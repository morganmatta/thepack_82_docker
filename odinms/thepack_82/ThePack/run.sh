#!/usr/bin/env bash

sed -i "s/MYSQL_HOST/$MYSQL_HOST/" db.properties
sed -i "s/ODINMS_CHANNEL_INTERFACE/$ODINMS_CHANNEL_INTERFACE/" world.properties
sed -i "s/ODINMS_WORLD_HOST/$ODINMS_WORLD_HOST/" login.properties
sed -i "s/ODINMS_WORLD_HOST/$ODINMS_WORLD_HOST/" channel.properties

../../jdk1.6.0_45/bin/java -cp '.:dist/*' -Dnet.sf.odinms.recvops=recvops.properties -Dnet.sf.odinms.sendops=sendops.properties -Dnet.sf.odinms.wzpath=wz -Djavax.net.ssl.keyStore=filename.keystore -Djavax.net.ssl.keyStorePassword=passwd -Djavax.net.ssl.trustStore=filename.keystore -Djavax.net.ssl.trustStorePassword=passwd net.sf.odinms.net.world.WorldServer &
sleep 10
../../jdk1.6.0_45/bin/java -cp '.:dist/*' -Dnet.sf.odinms.recvops=recvops.properties -Dnet.sf.odinms.sendops=sendops.properties -Dnet.sf.odinms.wzpath=wz -Dnet.sf.odinms.login.config=login.properties -Djavax.net.ssl.keyStore=filename.keystore -Djavax.net.ssl.keyStorePassword=passwd -Djavax.net.ssl.trustStore=filename.keystore -Djavax.net.ssl.trustStorePassword=passwd net.sf.odinms.net.login.LoginServer &
sleep 10
../../jdk1.6.0_45/bin/java -cp '.:dist/*' -Dnet.sf.odinms.recvops=recvops.properties -Dnet.sf.odinms.sendops=sendops.properties -Dnet.sf.odinms.wzpath=wz -Dnet.sf.odinms.channel.config=channel.properties -Djavax.net.ssl.keyStore=filename.keystore -Djavax.net.ssl.keyStorePassword=passwd -Djavax.net.ssl.trustStore=filename.keystore -Djavax.net.ssl.trustStorePassword=passwd net.sf.odinms.net.channel.ChannelServer -Dcom.sun.management.jmxremote.port=13373 -Dcom.sun.management.jmxremote.password.file=jmxremote.password -Dcom.sun.management.jmxremote.access.file=jmxremote.access &
